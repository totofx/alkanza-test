from django.conf.urls import url
from rest_app import views

urlpatterns = [
    url(r'^list_repos/$', views.ListRepos.as_view(), name='list_repos'),
    url(r'^list_repos_db/$', views.ListReposDB.as_view(), name='list_repos_db'),

    ]
