import mimetypes

import requests
import json
from django.core import serializers
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from rest_app.models import Repository
from django.http.response import HttpResponse


# Create your views here.


class ListRepos(TemplateView):
    template_name = 'list_repos.html'

    def get_context_data(self, **kwargs):
        response = requests.get(
                'https://api.github.com/search/repositories?q=stars:%3E1+user&location:colombia&sort=stars&order=desc&page=1&per_page=10')
        data = json.loads(response.text)
        context_data = super(ListRepos, self).get_context_data(**kwargs)
        context_data['customer_data_list'] = data.get('items')
        repository = Repository.objects.all()

        if not repository.exists():
            for r in context_data['customer_data_list']:
                p = Repository(id_repo=r.get('id'), name_repo=r.get('name'))
                p.save()

        return context_data


class ListReposDB(ListView):
    template_name = 'list_repos.html'
    model = Repository

    def get(self, request, *args, **kwargs):
        repos = Repository.objects.all()
        data = serializers.serialize('json', repos, fields=('id_repo', 'name_repo'))
        return HttpResponse(data, content_type='application/json')
