from django.db import models

# Create your models here.


class Repository(models.Model):
    id_repo = models.CharField(max_length=150, blank=True)
    name_repo = models.CharField(max_length=150, blank=True)
