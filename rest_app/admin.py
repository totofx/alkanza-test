from django.contrib import admin

# Register your models here.
from rest_app.models import Repository


class RepoAdmin(admin.ModelAdmin):
    list_display = ('id_repo', 'name_repo')


admin.site.register(Repository, RepoAdmin)
